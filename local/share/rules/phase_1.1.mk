# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:


IDENTITY ?= 0.9

logs:
	mkdir -p $@


# 03:50 3.8Gb   81.9% Reading reads.1.fastang reads.1.fasta
# Out of memory mymalloc(93323264), curr 3.99e+09 bytes
nr.fasta: logs all.transcripts.fasta
	$(call module_loader); \
	usearch -cluster_fast $^2 -id $(IDENTITY) -centroids $@ -uc centroids.uc \
	2>&1 \
	| tee $</usearch.$@.log

.META: centroids.uc
	1	Record type. H = the cluster assignment for the query; S = Centroid (clustering only); C = Cluster record (clustering only)	S
	2	Cluster number (0-based)	0
	3	Sequence length (S, N and H) or cluster size (C)	2556
	4	For H records, percent identity with target	100.0
	5	For H records, the strand: + or - for nucleotides, . for proteins	.
	6	Not used, parsers should ignore this field. Included for backwards compatibility	0
	7	Not used, parsers should ignore this field. Included for backwards compatibility	0
	8	Compressed alignment or the symbol '=' (equals sign). The = indicates that the query is 100% identical to the target sequence (field 10)	=
	9	Label of query sequence (always present)	asmbl_103465
	10	Label of target sequence (H records only)	asmbl_10862


centroids.uc: nr.fasta
	touch $@


.META: genes.centroids.tab
	1	genes	10000
	2	transcripts or comp83034_c2_seq3 Label of target sequence (H records only)	asmbl_10862
	3	Record type. H = the cluster assignment for the query; S = Centroid (clustering only); C = Cluster record (clustering only)	S
	4	Cluster number (0-based)	0
	5	Sequence length (S, N and H) or cluster size (C)	2556
	6	For H records, percent identity with target	100.0
	7	For H records, the strand: + or - for nucleotides, . for proteins	.
	8	Not used, parsers should ignore this field. Included for backwards compatibility	0
	9	Not used, parsers should ignore this field. Included for backwards compatibility	0
	10	Compressed alignment or the symbol '=' (equals sign). The = indicates that the query is 100% identical to the target sequence (field 10)	=
	11	Label of query sequence (always present)	asmbl_103465
	12	classifications of transcripts according to genome mapping status	InvalidQualityAlignment_YES_PASAmap

# map the centroids on genes on the bases of the transcripts for each gene
genes.centroids.tab: gene2trans.map centroids.uc
	translate -k -a -f 9 <(bawk '!/^C/ { if ( $$10 ~ /^*/ ) {$$10=$$9;} print $$0;}' <$^2) 2 <$< \
	| bsort -k 1,1 >$@

.META: genes2centroids.tab
	1	trinity genes	66439
	2	mapped centroids	asmbl_108854;asmbl_108847

# select genes that map on a certain number of centroids
genes2centroids.tab: genes.centroids.tab
	bawk '!/^[\#,$$]/ { print $$1,$$11; }' <$< \
	| bsort -k 1,1 \
	| collapsesets 2 \
	| bawk '!/^[\#,$$]/ { split($$2,a,";"); if ( length(a) <= $(TOLERANCE) ) print $$0 >"$@"; else  print $$0 >"genes.not2centroids.tab";}' >$@

# genes that map on more then a certain number of centroids
genes.not2centroids.tab: genes2centroids.tab
	touch $@

# it could be that more than one trinity gene maps on the same centroid. Here I
# collapse genes that map on the same centroid and devide the centroids tha map
# on just a certain number of genes from those which maps on more than that number
biunivocal.centroids2genes.tab: genes2centroids.tab
	bsort -k 2,2 <genes2centroids.tab \
	| collapsesets 1 \
	| select_columns 2 1 \
	| bawk '!/^[\#,$$]/ { split($$2,a,";"); if ( length(a) <= $(TOLERANCE) ) print $$0 >"$@"; else  print $$0 >"notbiunivocal.centroids2genes.tab";}' >$@


notbiunivocal.centroids2genes.tab: biunivocal.centroids2genes.tab
	touch $@

.PHONY: test
test:
	$(call module_loader); \
	usearch -derep_fulllength -help

ALL +=  logs \
	nr.fasta \
	centroids.uc \
	genes.centroids.tab \
	genes2centroids.tab \
	genes.not2centroids.tab \
	biunivocal.centroids2genes.tab \
	notbiunivocal.centroids2genes.tab

INTERMEDIATE +=

CLEAN +=

