# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:


MAKE := bmake

PASA_COMPREH_INIT_BUILD	?= 
EST			?= 
CDS_COMPLETE		?= 
CDS_PARTIAL		?= 

ID_START ?= 0.8
ID_END ?= 1.0
ID_STEP ?= 0.1

compreh_init_build.fasta:
	ln -sf $(PASA_COMPREH_INIT_BUILD) $@

cds_complete.fasta:
	ln -sf $(CDS_COMPLETE) $@

cds_partial.fasta:
	ln -sf $(CDS_PARTIAL) $@

est.fasta:
	ln -sf $(EST) $@

.META: gene2trans.map
	1	genes	10000
	2	transcripts	comp83034_c2_seq3
	3	classifications of transcripts according to genome mapping status	InvalidQualityAlignment_YES_PASAmap

gene2trans.map:
	ln -sf $(COMPREH_INIT_BUILD_DETAILS) $@

# all.transcripts.fasta: compreh_init_build.fasta cds_complete.fasta cds_partial.fasta est.fasta
# 	error_ignore \
# 	cat $^ \
# 	| fasta2tab \
# 	| head -n 10000 \
# 	| bsort \
# 	| tab2fasta 2 >$@

all.transcripts.fasta: compreh_init_build.fasta
	cat $^ >$@

# all.transcripts.fasta: compreh_init_build.fasta cds_complete.fasta cds_partial.fasta est.fasta
# 	cat $^ >$@


counts.tab: makefile all.transcripts.fasta gene2trans.map
	$(call module_loader); \
	RULES_PATH="$$PRJ_ROOT/local/share/rules/phase_1.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  >$@; \
	  for SEQ in $$(seq -s " " $(ID_START) $(ID_STEP) $(ID_END)) ; do \
	    mkdir -p $$SEQ; \
	    cd $$SEQ; \
	    ln -sf ../$<; \
	    ln -sf $$RULES_PATH rules.mk; \
	    ln -sf ../$^2; \
	    ln -sf ../$^3; \
	    $(MAKE) IDENTITY=$$SEQ; \
	    printf "$$SEQ\n" >>../$@; \
	    wc -l genes.not2centroids.tab genes2centroids.tab >>../$@; \
	    wc -l biunivocal.centroids2genes.tab notbiunivocal.centroids2genes.tab >>../$@; \
	    cd ..; \
	  done; \
	else \
	  printf "[ ERROR ] $$RULES_PATH not found!\n" >&2; \
	  exit 1; \
	fi


.PHONY: test
test:
	

ALL +=  compreh_init_build.fasta \
	cds_complete.fasta \
	cds_partial.fasta \
	est.fasta \
	all.transcripts.fasta \
	gene2trans.map \
	counts.tab

INTERMEDIATE +=

CLEAN +=

# add dipendence to clean targhet
clean: clean_dir

.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $$(seq -s " " $(ID_START) $(ID_STEP) $(ID_END)) ; do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	    rm -rf $$D; \
	  fi; \
	done